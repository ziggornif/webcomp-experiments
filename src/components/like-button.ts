type ColorsEnum = Record<
  string,
  {
    bg: string;
    hover: string;
  }
>;

class LikeButton extends HTMLElement {
  btnId: string;
  counter: number;
  colors: ColorsEnum;
  color: string;

  constructor() {
    super();

    this.btnId = `like-${Date.now()}`;
    this.counter = 0;
    this.attachShadow({ mode: 'open' });

    this.colors = {
      pink: {
        bg: '#EA4C89',
        hover: '#F082AC',
      },
      green: {
        bg: '#80D714',
        hover: '#BED714',
      },
    };
    this.color = this.getAttribute('color') ?? 'pink';

    this.#buildHTML();
  }

  #buildHTML() {
    const template = document.createElement('template');
    template.innerHTML = `
      <style>
        ${this.#style()}
      </style>
      <button id="${this.btnId}">
        <slot name="label"></slot>
        <span class="counter">${this.counter}</span>
      </button>
    `;
    this.shadowRoot?.appendChild(template.content.cloneNode(true));
  }

  #style() {
    return `
      button {
        background-color: ${this.colors[this.color].bg};
        border-radius: 8px;
        border-style: none;
        box-sizing: border-box;
        color: #FFFFFF;
        cursor: pointer;
        display: inline-block;
        font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 14px;
        font-weight: 500;
        height: 40px;
        line-height: 20px;
        list-style: none;
        margin: 0;
        outline: none;
        padding: 10px 16px;
        position: relative;
        text-align: center;
        text-decoration: none;
        transition: color 100ms;
        vertical-align: baseline;
        user-select: none;
        -webkit-user-select: none;
        touch-action: manipulation;
      }

      button:hover {
        background-color: ${this.colors[this.color].hover};
      }
    `;
  }

  clickHandler() {
    const counterElem = this.shadowRoot?.querySelector('.counter');
    if (!counterElem) return;
    this.counter++;
    counterElem.textContent = this.counter.toString();
  }

  /**
   * Runs each time the element is appended to or moved in the DOM
   */
  connectedCallback() {
    const btn = this.shadowRoot?.querySelector('button');
    if (!btn) return;
    btn.addEventListener('click', this.clickHandler.bind(this));
  }

  /**
   * Runs when the element is removed from the DOM
   */
  disconnectedCallback() {
    const btn = this.shadowRoot?.querySelector('button');
    if (!btn) return;
    btn.removeEventListener('click', this.clickHandler);
  }
}

export { LikeButton };
