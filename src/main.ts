/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { LikeButton } from './components/like-button';

customElements.define('like-button', LikeButton);

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div>
    <like-button></like-button>
    <like-button color="green"></like-button>
    <like-button><span slot="label">❤️ Likes</span></like-button>
    <like-button color="green"><span slot="label">Click me !</span></like-button>
  </div>
`;
